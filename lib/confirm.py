import os
import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import json
from bson.objectid import ObjectId
import pprint
from datetime import date, datetime
import traceback

import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar


class ConfirmWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent
        
        titlebox= QVBoxLayout()
        layout = QVBoxLayout()
        button_box = QHBoxLayout()

        label_title = QLabel()
        label_title.setText('<center><font size="5">Confirm before uploading to the database</font></center>')
        label_practice = QLabel()
        label_practice.setText('<center><font size="4" color = "green"> Practice Mode</font></center>')
        Upload_button= QPushButton("&Upload!")
        Upload_button.clicked.connect(self.upload_to_db)
        json_button= QPushButton("&Check json (for expert)")
        json_button.clicked.connect(self.check_json)
        back_button = QPushButton('&Back')
        back_button.clicked.connect(self.back_page)
        
        titlebox.addWidget(label_title)

        button_box.addWidget(back_button)
        button_box.addStretch()
        button_box.addWidget(json_button)
        button_box.addWidget(Upload_button)

        inner = QScrollArea()
        inner.setFixedHeight(400)
        inner.setFixedWidth(810)
        result_wid = QWidget()
        result_wid.setLayout( self.make_layout() )

        inner.setWidgetResizable(True)
        inner.setWidget(result_wid)

        layout.addLayout(titlebox)
        layout.addWidget(inner)
        layout.addLayout(button_box)
        self.setLayout(layout)

    def back_page(self):
        self.parent.back_to_test()

    def check_json(self):
        self.parent.confirm_json()

    def upload_to_db(self):
        self.parent.upload_to_db()

    def add_info(self,Form_layout,label_str,form_text):
        
        label = QLabel()
        label.setText(label_str)

        if label_str =='Comment :':
            inner = QTextEdit()
            inner.setText(form_text)
            inner.setReadOnly(True)
            inner.setFocusPolicy(Qt.NoFocus)
            inner.setStyleSheet("background-color : linen")
            
            editor = QScrollArea()
            editor.setWidgetResizable(True)
            editor.setWidget(inner)

        else:
            editor = QLineEdit()
            editor.setText(form_text)
            editor.setReadOnly(True)
            editor.setFocusPolicy(Qt.NoFocus)
            editor.setStyleSheet("background-color : linen")
        #        editor.setStyleSheet("background-color : azure")

        Form_layout.addRow(label,editor)

#################################################################        
    def make_layout(self):
        
        if self.parent.info_dict['componentType'] == "MODULE":
            return self.layout_ModuleQC()
        elif self.parent.info_dict['componentType'] == 'practice':
            return self.layout_ModuleQC()
        else:
            return self.layout_ModuleQC()

    def layout_ModuleQC(self):

        HBox= QHBoxLayout()
        
        Form_layout = QFormLayout()
        self.add_info(Form_layout,'Current Stage :',self.parent.result_dict['localDB']['currentStage'])
        self.add_info(Form_layout,'Test Type :',self.parent.result_dict['localDB']['testType'])
        
        self.add_info(Form_layout,'Comment :',self.parent.result_dict['localDB']['results']['comment'])

        I=[]
        V=[]
        I_error=[]
        for Event in self.parent.result_dict['localDB']['results']["Sensor_IV"]:
            I.append( Event["Current_mean"] )
            V.append( Event["Voltage"] )
            I_error.append( Event["Current_sigma"] )
            
        if self.parent.result_dict['localDB']['results']["unit"]["Current"] == 'uA':
            I_unit = "Current [$\mu$A]"
        else:
            I_unit = "Current [" + self.parent.result_dict['localDB']['results']["unit"]["Current"] +"]"
        V_unit ="Voltage [" + self.parent.result_dict['localDB']['results']["unit"]["Voltage"] +"]"

        graph = Create_Canvas(self)
        graph.setFixedHeight(380)
        graph.setFixedWidth(400)
        graph.graph_plot(V,I,V_unit,I_unit,I_error)
        graph.canvas_show()
        
        HBox.addLayout(Form_layout)
        HBox.addWidget(graph)

        return HBox

class Create_Canvas(QWidget):
    def __init__(self, parent):
        super(QWidget, self).__init__(parent)
        self.parent = parent
        
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        toolbar = NavigationToolbar(self.canvas, self)
        
        layout = QVBoxLayout()
        layout.addWidget(self.canvas)
        layout.addWidget(toolbar)
        self.setLayout(layout)

    def graph_plot(self,x_list,y_list,x_unit,y_unit,y_error_list):
        self.ax = self.figure.add_subplot(111)
        self.ax.scatter(x_list,y_list,c='black')
        self.ax.errorbar(x_list,y_list,yerr=y_error_list,capsize=3,ecolor='black',c='black')
        self.ax.set_title("Sensor I-V")
        self.ax.set_xlabel(x_unit)
        self.ax.set_ylabel(y_unit)
        self.figure.subplots_adjust(bottom=0.15)
        self.figure.subplots_adjust(left=0.25)

    def canvas_show(self):
        self.canvas.draw()
