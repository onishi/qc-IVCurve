import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import json
class InitialWindow(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__(parent)
        self.parent = parent


        layout = QVBoxLayout()
        bottom_box = QHBoxLayout()
        grid_edit = QGridLayout()

        HBox1 = QHBoxLayout()
        HBox2 = QHBoxLayout()
        
        label_title = QLabel()
        label_title.setText('<center><font size="7">Choose result file</font></center>')

        Next_button= QPushButton("&Next")
        Next_button.clicked.connect(self.pass_result)
        Back_button= QPushButton("&Back")
        Back_button.clicked.connect(self.back_page)

        Choose_button= QPushButton("Choose file")
        Choose_button.clicked.connect(self.choose_file)

        bottom_box.addWidget(Back_button)
        bottom_box.addStretch()
        bottom_box.addWidget(Next_button)


        label_result = QLabel()
        label_result.setText('Result file : ')
        label_comment = QLabel()
        label_comment.setText('Comment : ')
        
        self.edit_comment = QTextEdit()
        self.edit_comment.setText(self.parent.result_info['comment'])
        self.edit_result = QLineEdit()
        self.edit_result.setFocusPolicy(Qt.NoFocus)
        self.edit_result.setReadOnly(True)
        if not self.parent.result_info['filename'] =='':
            self.edit_result.setText(self.parent.result_info['filename'])
        else:
            self.edit_result.setText('')

        HBox1.addWidget(self.edit_result)
        HBox1.addWidget(Choose_button)

        grid_edit.addWidget(label_result,0,0)
        grid_edit.addLayout(HBox1,0,1)

        grid_edit.addWidget(label_comment,1,0)

        outer = QScrollArea()
        outer.setWidgetResizable(True)
        outer.setWidget(self.edit_comment)
        HBox2.addWidget(outer)
        grid_edit.addWidget(outer,1,1)



        layout.addWidget(label_title)
        layout.addStretch()
        layout.addLayout(grid_edit)
        layout.addLayout(bottom_box)

        self.setLayout(layout)

    def choose_file(self):

        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)

        try:
            dlg.exec_()
            filename =  dlg.selectedFiles()[0] 
            self.edit_result.setText(filename)
        except:
            import traceback
            print (traceback.format_exc() )
            QMessageBox.warning(None, 'Warning','Please choose correct file', QMessageBox.Ok)

    def pass_result(self):
        try:
            self.parent.result_info['filename'] = self.edit_result.text()
            with open(self.edit_result.text()) as f:
                result_dict = json.load(f)
            free_comment = self.edit_comment.toPlainText()

            result_dict["V_step"] = float(result_dict["V_step"])
            self.parent.recieve_result(result_dict,free_comment)
        except:
            import traceback
            print (traceback.format_exc() )
            QMessageBox.warning(None, 'Warning','Please choose correct file', QMessageBox.Ok)

    def back_page(self):
        self.parent.close_and_return()

