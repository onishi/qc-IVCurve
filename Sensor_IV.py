import os
import shutil
import sys
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.Qt import *
import sip
import cv2
import numpy as np
import json
import pymongo
from bson.objectid import ObjectId

sys.path.append(os.path.join(os.path.dirname(__file__), './lib'))
import initial_Sensor_IV

class TestWindow(QMainWindow):
############################################################################################
    def __init__(self, parent=None):
        super(QMainWindow, self).__init__()
        self.parent = parent

        self.setWindowTitle('Sensor I-V')
        self.scale_window(510,255)
            
        self.result_info = {
            'comment':'',
            'filename':''

        }

    def recieve_result(self,result,comment):
        self.result_info['result'] = result
        self.result_info['comment'] = comment

        self.fill_result()
        self.return_result()

    def fill_result(self):
        self.test_result_dict = {
            'results':{
                'localDB' :self.result_info['result'],
                'ITkPD'   :self.result_info['result'],
                'summary' :self.result_info['result']
            }
        }
        #print(self.test_result_dict)
        self.test_result_dict['results']['localDB']['comment'] = self.result_info['comment']
        self.test_result_dict['results']['ITkPD']['comment'] = self.result_info['comment']
        self.test_result_dict['results']['summary']['comment'] = self.result_info['comment']
        
        print('[Test Result file] ' + self.result_info['filename'] )

###########################################################################################

    def init_ui(self):
        self.initial_wid = initial_Sensor_IV.InitialWindow(self)
        self.update_widget(self.initial_wid)

    def scale_window(self,x,y):
        self.setGeometry(0, 0, x, y)

    def update_widget(self, w):
        self.scale_window(425,255)
        self.setCentralWidget(w)
        self.show()

    def close_and_return(self):
        self.close()
        self.parent.back_from_test()
       
    def back_page(self):
        self.parent.init_ui()

    def back_window(self):
        self.parent.recieve_backpage()
        
    def call_another_window(self,window):
        self.hide()
        window.init_ui()

    def return_result(self):
        self.parent.recieve_result(self,self.test_result_dict)
